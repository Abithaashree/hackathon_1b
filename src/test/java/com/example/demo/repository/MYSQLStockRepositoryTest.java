package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entities.Stock;

@SpringBootTest
public class MYSQLStockRepositoryTest {
	
	@Autowired
	MYSQLStockRepository mySQLStockRepository;
	
	/*
	 * @Test public void testGetAllStocks() { List<Stock> returnedList =
	 * mySQLStockRepository.getAllStocks(); assert(returnedList != null); }
	 */
	@Test
	public void testgetAllStocks() {
		List <Stock> returnedList = mySQLStockRepository.getAllStocks();
		assertThat(returnedList).isNotEmpty();
		
		for(Stock stock: returnedList) {
			System.out.println("stock ticker:" + stock.getStockTicker());
			System.out.println("Stock id:" + stock.getId());
			System.out.println("Stock Buy/Sell:" + stock.getBuyOrSell());
			System.out.println("Stock Price:" + stock.getPrice());
			System.out.println("Stock Status Code:" + stock.getStatusCode());
			System.out.println("Stock Volume:" + stock.getVolume());
			
			stock.setStockTicker("LMN");
			stock.setId(2);
			stock.setBuyOrSell("Buy");
			stock.setPrice(12.34);
			stock.setStatusCode(0);
			stock.setVolume(50);
		}
		
	}
	
	

}
