function getAllStocks(){
	const url="http://localhost:8081/api/trade/";
	fetch(url)//promise object to return data from Rest API
		.then(response => { return response.json();}) //resolve , data from resolve is passed to next then
		.then(stocks => {			
			if (stocks.length > 0) {
				 var temp = "";
				 stocks.forEach((itemData) => {
					 temp += "<tr>";
					 temp += "<td>" + itemData.id + "</td>";
					 temp += "<td>" + itemData.stockTicker + "</td>";
				   temp += "<td>" + itemData.price + "</td>";
				  temp += "<td>" + itemData.volume + "</td>";
				});
				 document.getElementById('tbodyAllStocks').innerHTML = temp; 
				 }	
			})
			}		



function buyStock2(){
	const data={id:1,
	           stockTicker:"APPL(APPLE INC)",
	           price:50,
		      volume:document.getElementById('appleQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/buy', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}

function sellStock2(){
	const data={id:1,
	           stockTicker:"APPL(APPLE INC)",
	           price:50,
		      volume:document.getElementById('appleQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/sell', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}

function buyCiti(){
	const data={id:2,
	           stockTicker:"C(Citigroup INC)",
	           price:100,
		      volume:document.getElementById('citiQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/buy', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}
function sellCiti(){
	const data={id:2,
	           stockTicker:"C(Citigroup INC)",
	           price:100,
		      volume:document.getElementById('citiQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/sell', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}


function buyNetflix(){
	const data={id:3,
	           stockTicker:"NFLX(Netflix INC)",
	           price:67,
		      volume:document.getElementById('netflixQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/buy', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}

function sellNetflix(){
	const data={id:3,
	           stockTicker:"NFLX(Netflix INC)",
	           price:67,
		      volume:document.getElementById('netflixQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/sell', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}


function buyAdobe(){
	const data={id:4,
	           stockTicker:"ADBE(Adobe INC)",
	           price:90,
		      volume:document.getElementById('adobeQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/buy', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}

function sellAdobe(){
	const data={id:4,
	           stockTicker:"ADBE(Adobe INC)",
	           price:90,
		      volume:document.getElementById('adobeQuantity').value};

alert(JSON.stringify(data));
//POST request with body equal on data in JSON format
fetch('http://localhost:8081/api/trade/sell', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
})
.then((response) => response.json())
//Then with the data from the response in JSON...
.then((data) => {
  console.log('Success:', data);
  getAllStocks();
}
)
//Then with the error genereted...
.catch((error) => {
  console.error('Error:', error);
});
}


