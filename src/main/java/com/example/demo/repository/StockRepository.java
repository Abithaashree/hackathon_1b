package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Stock;

@Component
public interface StockRepository {
	public List<Stock> getAllStocks();
	
	public Stock getStockById(int id);
	
	public Stock getStockByTicker(String stockTicker);

	public Stock editStock(Stock stock);
	
	public Stock buyStock(Stock stock);
	
	public Stock sellStock(Stock stock);

	public int deleteStock(int id);

	public Stock addStock(Stock stock);
}