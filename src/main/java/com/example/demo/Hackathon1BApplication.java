package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hackathon1BApplication {

	public static void main(String[] args) {
		SpringApplication.run(Hackathon1BApplication.class, args);
	}

}
