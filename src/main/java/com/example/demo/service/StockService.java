package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stock;
import com.example.demo.repository.StockRepository;

@Service
public class StockService {
	@Autowired
	private StockRepository repository;

	public List<Stock> getAllStocks() {
		return repository.getAllStocks();
	}
	
	public Stock getStock(int id) {
		return repository.getStockById(id);
	}
	public Stock getStock(String stockTicker) {
		return repository.getStockByTicker(stockTicker);
	}

	public Stock saveStock(Stock stock) {
		return repository.editStock(stock);
	}

	public Stock newStock(Stock stock) {
		return repository.addStock(stock);
	}
	
	public Stock buyStock(Stock stock) {
		return repository.buyStock(stock);
	}
	
	public Stock sellStock(Stock stock) {
		return repository.sellStock(stock);
	}

	public int deleteStock(int id) {
		return repository.deleteStock(id);
	}
}