package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Stock;
import com.example.demo.service.StockService;

@RestController
@RequestMapping(value="api/trade",produces = "application/json",method={RequestMethod.GET, RequestMethod.PUT})

public class TradeController {
	@Autowired
	StockService service;

	@GetMapping(value = "/")
	public List<Stock> getAllStocks() {
		return service.getAllStocks();
	}

	@GetMapping(value = "/{stockTicker}")
	public Stock getStockByTicker(@PathVariable("stockTicker") String stockTicker) {
		return service.getStock(stockTicker);
	}
	
	//http://localhost:8081/api/trade/buy/100/1
	@PostMapping(value = "/buy")
	public Stock buyStock(@RequestBody Stock stock) {
		return service.buyStock(stock);
	}
	
	@PostMapping(value = "/sell")
	public Stock sellStock(@RequestBody Stock stock ) {
		return service.sellStock(stock);
	}
	
	
	

	

	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
